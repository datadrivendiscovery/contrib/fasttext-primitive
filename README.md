This is a D3M primitive for FastText's [text classification](https://fasttext.cc/docs/en/supervised-tutorial.html)

This primitive was evaluated on two D3M datasets: JIDO_SOHR_Articles_1061 and JIDO_SOHR_Tab_Articles_8569

## Results

### JIDO_SOHR_Articles_1061
```
F1 Score: 0.9204318272690925
```

### JIDO_SOHR_Tab_Articles_8569
```
F1 Score: 0.9226338265964842
```