import os.path
import fasttext
import csv
import numpy as np
import os

from typing import Optional, List, Tuple, Any, Sequence
from d3m import container
from d3m.base import utils as base_utils
from d3m.container import DataFrame as d3m_dataframe
from d3m.primitive_interfaces.base import CallResult, DockerContainer
from d3m.primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
from d3m.metadata import base as metadata_base
from d3m.metadata import hyperparams, params
from collections import OrderedDict
from fasttextpickle import PickleableFastText

__all__ = ('FastTextWrapperPrimitive',)

Inputs = container.DataFrame
Outputs = container.DataFrame

class Params(params.Params):
    clf: Optional[Any]
    target_columns_metadata_: Optional[List[OrderedDict]]
    target_names_: Optional[Sequence[Any]]
    target_column_indices_: Optional[Sequence[int]]

class Hyperparams(hyperparams.Hyperparams):
    # Bounded means bounded on only one end; uniformInt is bounded on both ends
    learning_rate = hyperparams.Uniform(
        lower=0.0,
        upper=1.0,
        default=0.1,
        semantic_types=[
        'https://metadata.datadrivendiscovery.org/types/TuningParameter'
        ],
        description="learning rate"
    )
    dim_word_vectors = hyperparams.UniformInt(
        lower=10,
        upper=1000,
        default=100,
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter'
        ],
        description="size of word vectors"
    )
    window_size = hyperparams.Bounded[int](
        lower=1,
        upper=None,
        default=5,
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter'
        ],
        description="size of the context window"
    )
    n_epoch = hyperparams.Bounded[int](
        lower=1,
        upper=None,
        default=15,
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter'
        ],
        description="number of epochs"
    )
    minCount = hyperparams.Bounded[int](
        lower=1,
        upper=None,
        default=1,
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter'
        ],
        description="minimal number of word occurences"
    )
    minCountLabel = hyperparams.Bounded[int](
        lower=1,
        upper=None,
        default=1,
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter'
        ],
        description="minimal number of label occurences"
    )
    min_n = hyperparams.Bounded[int](
        lower=0,
        upper=None,
        default=0,
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter'
        ],
        description="min length of char ngram"
    )
    max_n = hyperparams.Bounded[int](
        lower=0,
        upper=None,
        default=0,
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter'
        ],
        description="max length of char ngram"
    )
    neg = hyperparams.Bounded[int](
        lower=0,
        upper=None,
        default=5,
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter'
        ],
        description="number of negatives sampled"
    )
    maxlen_word_ngram = hyperparams.Bounded[int](
        lower=0,
        upper=None,
        default=1,
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter'
        ],
        description="max length of word ngram"
    )
    loss = hyperparams.Enumeration[str](
        values=['ns', 'hs', 'softmax', 'ova'],
        default='softmax',
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter'
        ],
        description="loss function"
    )
    n_bucket = hyperparams.Bounded[int](
        lower=0,
        upper=None,
        default=2000000,
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter'
        ],
        description="number of buckets"
    )
    n_thread = hyperparams.Bounded[int](
        lower=1,
        upper=None,
        default=os.cpu_count(),
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ResourcesUseParameter'
        ],
        description="number of threads"
    )
    lrUpdateRate = hyperparams.Bounded[int](
        lower=1,
        upper=None,
        default=100,
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter'
        ],
        description="change the rate of updates for the learning rate"
    )
    sampling_threshold = hyperparams.Bounded[float](
        lower=0,
        upper=None,
        default=0.0001,
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter'
        ],
        description="sampling threshold"
    )
    use_inputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        description="A set of inputs column indices to force primitive to operate on. If any specified column cannot be used, it is skipped.",
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter'
        ]
    )
    exclude_inputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        description="A set of inputs column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter'
        ],
    )
    use_outputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of outputs column indices to force primitive to operate on.",
    )
    exclude_outputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of outputs column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
    )
    return_result = hyperparams.Enumeration(
        values=['append', 'replace', 'new'],
        # Default value depends on the nature of the primitive.
        default='new',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Defines if result columns should append or replace original columns",
    )
    use_semantic_types = hyperparams.UniformBool(
        default=False,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Controls whether semantic_types metadata will be used for filtering columns in input dataframe.",
    )
    add_index_columns = hyperparams.UniformBool(
        default=True,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Also include primary index columns if input data has them. Applicable only if \"return_result\" is set to \"new\".",
    )
    error_on_no_input = hyperparams.UniformBool(
        default=True,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Throw an exception if no input column is selected/provided.",
    )
    return_semantic_type = hyperparams.Enumeration[str](
        values=[
            'https://metadata.datadrivendiscovery.org/types/Attribute',
            'https://metadata.datadrivendiscovery.org/types/ConstructedAttribute',
            'https://metadata.datadrivendiscovery.org/types/PredictedTarget'
        ],
        default='https://metadata.datadrivendiscovery.org/types/PredictedTarget',
        description='Decides what semantic type to attach to generated output',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter']
    )

class FastText(SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
        FastText Primitive
    """
    __author__ = "JPL MARVIN"
    metadata = metadata_base.PrimitiveMetadata({
        "algorithm_types": [metadata_base.PrimitiveAlgorithmType.BAG_OF_TRICKS],
        "name": "FastText text classifier",
        "primitive_family": metadata_base.PrimitiveFamily.NATURAL_LANGUAGE_PROCESSING,
        "python_path": "d3m.primitives.natural_language_processing.BAG_OF_TRICKS.Fasttext",
        "source": {'name': 'JPL', 'contact': 'mailto:luciana.cendon@jpl.nasa.gov'},
        "version": "0.1",
        "id": "0dcaa3c4-a325-4b3a-a037-f18b8d70d29c"
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 _verbose: int = 0,
                 temporary_directory: str = None) -> None:
        super().__init__(hyperparams=hyperparams, random_seed=random_seed, temporary_directory=temporary_directory)

        self._clf = fasttext

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        self._inputs = inputs
        self._outputs = outputs
        self._fitted = False
        self._new_training_data = True

        # Remove the "\n" from text
        self._inputs.iloc[:, 1] = self._inputs.iloc[:, 1].replace(r'\n',' ', regex=True)

        # Creates file with training data
        self._create_training_file()

    def _create_training_file(self) -> None:

        # Saving txt file with fasttext format
        if not os.path.exists(self.temporary_directory):
            os.makedirs(self.temporary_directory)

        x_train = ('__label__' + self._outputs['articleofinterest'].astype(str) + ' ' + self._inputs.iloc[:, 1]).astype(str)
        x_train.to_csv(os.path.join(self.temporary_directory, "ft_train.txt"),
                       index=False, sep=' ',
                       header=False,
                       quoting=csv.QUOTE_NONE,
                       quotechar="",
                       escapechar=" ")

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        if self._inputs is None or self._outputs is None:
            raise ValueError("Missing training data.")
        if not self._new_training_data:
            return CallResult(None)

        self._new_training_data = False
        self._training_inputs, self._training_indices = self._get_columns_to_fit(self._inputs, self.hyperparams)
        self._training_outputs, self._target_names, self._target_column_indices = self._get_targets(self._outputs, self.hyperparams)
        self._input_column_names = self._training_inputs.columns.astype(str)

        if len(self._training_indices) > 0 and len(self._target_column_indices) > 0:
            self._target_columns_metadata = self._get_target_columns_metadata(self._training_outputs.metadata, self.hyperparams)

            self._clf = self._clf.train_supervised(input=os.path.join(self.temporary_directory, "ft_train.txt"),
                                                    lr=self.hyperparams['learning_rate'],
                                                    dim=self.hyperparams['dim_word_vectors'],
                                                    ws=self.hyperparams['window_size'],
                                                    epoch=self.hyperparams['n_epoch'],
                                                    minCount=self.hyperparams['minCount'],
                                                    minCountLabel=self.hyperparams['minCountLabel'],
                                                    minn=self.hyperparams['min_n'],
                                                    maxn=self.hyperparams['max_n'],
                                                    neg=self.hyperparams['neg'],
                                                    wordNgrams=self.hyperparams['maxlen_word_ngram'],
                                                    loss=self.hyperparams['loss'],
                                                    bucket=self.hyperparams['n_bucket'],
                                                    thread=self.hyperparams['n_thread'],
                                                    lrUpdateRate=self.hyperparams['lrUpdateRate'],
                                                    t=self.hyperparams['sampling_threshold'])
            self.fitted = True
        else:
            if self.hyperparams['error_on_no_input']:
                raise RuntimeError("No input columns were selected")
            self.logger.warn("No input columns were selected")

        return CallResult(None)

    def get_params(self) -> Params:
        # For now returns None because Params is empty
        return Params(clf=PickleableFastText(self._clf),
                      target_columns_metadata_= self._target_columns_metadata,
                      target_names_ = self._target_names,
                      target_column_indices_ = self._target_column_indices)

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:

        inputs.iloc[:, 1] = inputs.iloc[:, 1].replace(r'\n', ' ', regex=True)
        ft_inputs, columns_to_use = self._get_columns_to_fit(inputs, self.hyperparams)

        if len(ft_inputs.columns):
            ft_output = self._clf.predict(inputs.iloc[:, 1].tolist())
            # ft_ouput[0]: labels ; ft_output[1]: probability score
            output = self._wrap_predictions(inputs, ft_output[0])
            output.columns = self._target_names
            output = [output]
        else:
            if self.hyperparams['error_on_no_input']:
                raise RuntimeError("No input columns were selected")
            self.logger.warn("No input columns were selected")

        outputs = base_utils.combine_columns(return_result=self.hyperparams['return_result'],
                                            add_index_columns=self.hyperparams['add_index_columns'],
                                            inputs=inputs, column_indices=self._target_column_indices,
                                            columns_list=output)
        return CallResult(outputs)

    def set_params(self, *, params: Params) -> None:
        self._clf = params['clf']
        self._target_columns_metadata = params['target_columns_metadata_']
        self._target_names = params['target_names_']
        self._target_column_indices = params['target_column_indices_']

    def _wrap_predictions(self, inputs: Inputs, predictions: List[str]) -> container.DataFrame:
        preds = np.array([p[0].split('__label__')[-1] for p in predictions], dtype=int)
        outputs = d3m_dataframe(preds, generate_metadata=False)
        outputs.metadata = self._update_predictions_metadata(inputs.metadata, outputs, self._target_columns_metadata)
        return outputs

    @classmethod
    def _get_columns_to_fit(cls, inputs: Inputs, hyperparams: Hyperparams):
        if not hyperparams['use_semantic_types']:
            return inputs, list(range(len(inputs.columns)))

        inputs_metadata = inputs.metadata

        def can_produce_column(column_index: int) -> bool:
            return cls._can_produce_column(inputs_metadata, column_index, hyperparams)

        columns_to_produce: List[Any] = []

        columns_to_produce, columns_not_to_produce = base_utils.get_columns_to_use(
            inputs_metadata,
            use_columns=hyperparams['use_inputs_columns'],
            exclude_columns=hyperparams['exclude_inputs_columns'],
            can_use_column=can_produce_column
        )
        return inputs.iloc[:, columns_to_produce], columns_to_produce

    @classmethod
    def _update_predictions_metadata(cls, inputs_metadata: metadata_base.DataMetadata, outputs: Optional[Outputs],
                                     target_columns_metadata: List[OrderedDict]) -> metadata_base.DataMetadata:
        outputs_metadata = metadata_base.DataMetadata().generate(value=outputs)

        for column_index, column_metadata in enumerate(target_columns_metadata):
            column_metadata.pop("structural_type", None)
            outputs_metadata = outputs_metadata.update_column(column_index, column_metadata)

        return outputs_metadata

    @classmethod
    def _can_produce_column(cls, inputs_metadata: metadata_base.DataMetadata, column_index: int, hyperparams: Hyperparams) -> bool:
        column_metadata = inputs_metadata.query((metadata_base.ALL_ELEMENTS, column_index))

        #accepted_structural_types = (str)
        accepted_semantic_types = set()
        #accepted_semantic_types.add("https://metadata.datadrivendiscovery.org/types/Attribute")
        accepted_semantic_types.add( "http://schema.org/Text")
        if not issubclass(column_metadata['structural_type'], accepted_structural_types):
            return False

        semantic_types = set(column_metadata.get('semantic_types', []))

        if len(semantic_types) == 0:
            cls.logger.warning("No semantic types found in column metadata")
            return False
        # Making sure all accepted_semantic_types are available in semantic_types
        if len(accepted_semantic_types - semantic_types) == 0:
            return True

        return False

    @classmethod
    def _get_targets(cls, data: container.DataFrame, hyperparams: Hyperparams) -> Tuple[container.DataFrame, list, Any]:
        if not hyperparams['use_semantic_types']:
            return data, list(data.columns), list(range(len(data.columns)))
        metadata = data.metadata

        def can_produce_column(column_index: int) -> bool:
            accepted_semantic_types = set()
            accepted_semantic_types.add("https://metadata.datadrivendiscovery.org/types/TrueTarget")
            column_metadata = metadata.query((metadata_base.ALL_ELEMENTS, column_index))
            semantic_types = set(column_metadata.get('semantic_types', []))
            if len(semantic_types) == 0:
                cls.logger.warning("No semantic types found in column metadata")
                return False
            # Making sure all accepted_semantic_types are available in semantic_types
            if len(accepted_semantic_types - semantic_types) == 0:
                return True
            return False

        target_column_indices, target_columns_not_to_produce = base_utils.get_columns_to_use(
            metadata,
            use_columns=hyperparams['use_outputs_columns'],
            exclude_columns=hyperparams['exclude_outputs_columns'],
            can_use_column=can_produce_column
        )
        targets: container.DataFrame
        if target_column_indices:
            targets = data.select_columns(target_column_indices)
        target_column_names = []
        for idx in target_column_indices:
            target_column_names.append(data.columns[idx])
        return targets, target_column_names, target_column_indices

    @classmethod
    def _get_target_columns_metadata(cls, outputs_metadata: metadata_base.DataMetadata, hyperparams: Hyperparams) -> List[OrderedDict]:
        outputs_length = outputs_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']

        target_columns_metadata: List[OrderedDict] = []
        for column_index in range(outputs_length):
            column_metadata = OrderedDict(outputs_metadata.query_column(column_index))

            # Update semantic types and prepare it for predicted targets.
            semantic_types = set(column_metadata.get('semantic_types', []))
            semantic_types_to_remove = set(["https://metadata.datadrivendiscovery.org/types/TrueTarget", "https://metadata.datadrivendiscovery.org/types/SuggestedTarget"])
            add_semantic_types = set(["https://metadata.datadrivendiscovery.org/types/PredictedTarget"])
            add_semantic_types.add(hyperparams["return_semantic_type"])
            semantic_types = semantic_types - semantic_types_to_remove
            semantic_types = semantic_types.union(add_semantic_types)
            column_metadata['semantic_types'] = list(semantic_types)

            target_columns_metadata.append(column_metadata)

        return target_columns_metadata