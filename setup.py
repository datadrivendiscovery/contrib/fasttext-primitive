import os
from setuptools import setup, find_packages

PACKAGE_NAME = ''


def read_package_variable(key):
    """Read the value of a variable from the package without importing."""
    module_path = os.path.join(PACKAGE_NAME, '__init__.py')
    with open(module_path) as module:
        for line in module:
            parts = line.strip().split(' ')
            if parts and parts[0] == key:
                return parts[-1].strip("'")
    assert False, "'{0}' not found in '{1}'".format(key, module_path)


setup(
    name='prim',
    version=read_package_variable('__version__'),
    license="Apache-2.0",
    description='FastText Text Classification primitive',
    author=read_package_variable('__author__'),
    packages=find_packages(exclude=['contrib', 'docs', 'tests*']),
    install_requires=[
        'd3m', 
        'fasttext==0.9.2'
    ],
    url='https://gitlab.com/datadrivendiscovery/contrib/fasttext-primitive',
    entry_points = {
        'd3m.primitives': [
            'natural_language_processing.BAG_OF_TRICKS.Fasttext = fasttext_prim:FastText'
        ],
    },
)
